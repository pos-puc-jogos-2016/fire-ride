package br.net.gavazzoni.horse.game;


import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.MotionEvent;

abstract class AbstractScene {

    protected float xFactor;
    protected float yFactor;
    protected Paint textPaint;
    protected GameState state;

    public AbstractScene(float xFactor, float yFactor) {
        this.xFactor = xFactor;
        this.yFactor = yFactor;
    }

    public AbstractScene(float xFactor, float yFactor, Paint textPaint) {
        this.xFactor = xFactor;
        this.yFactor = yFactor;
        this.textPaint = textPaint;
    }

    public abstract boolean process(MotionEvent event);
    public abstract void draw(Canvas canvas);
    public abstract void update(float deltaTime);

    public GameState getState() {
        return this.state;
    }
}
