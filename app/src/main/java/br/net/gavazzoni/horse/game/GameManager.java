package br.net.gavazzoni.horse.game;


import android.graphics.Point;

class GameManager {
    float money = 1000.0f;
    int totalRaces = 5;
    int countRace = 1;

    float betAmount = 0.0f;
    int winnerHorse = 0;
    int betHorse = 0;
    Point screenSize;

    GameState state = GameState.Bet;

    private static final GameManager ourInstance = new GameManager();

    public static GameManager getInstance() {
        return ourInstance;
    }

    private GameManager() {
    }

    public void reset() {
        this.money = 1000.0f;
        this.totalRaces = 5;
        this.countRace = 1;

        this.betAmount = 0.0f;
        this.winnerHorse = 0;
        this.betHorse = 0;
    }
}
