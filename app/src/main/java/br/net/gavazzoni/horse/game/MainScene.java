package br.net.gavazzoni.horse.game;


import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.provider.Settings;
import android.util.Log;
import android.view.MotionEvent;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

import br.net.gavazzoni.horse.model.Highscore;

public class MainScene extends AbstractScene {

    private static final GameManager gameManager = GameManager.getInstance();
    private final HUD hud;
    private final Context context;
    private Paint winnerPaint;
    private Rect textBounds = new Rect();

    private ArrayList<Horse> horses = new ArrayList<>();

    public MainScene(float xFactor, float yFactor, Paint paint, Context context) throws JSONException {
        super(xFactor, yFactor);

        hud = new HUD(xFactor, yFactor, paint);

        AssetManager assetManager = context.getAssets();
        Bitmap sprite = null;
        String atlas = "";
        this.context = context;

        try {
            InputStream inputStream = assetManager.open("sprites/horses.png");
            sprite = BitmapFactory.decodeStream(inputStream);
            inputStream.close();

            InputStream jsonStream = assetManager.open("sprites/horses.json");
            BufferedReader reader = new BufferedReader(new InputStreamReader(jsonStream));

            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line).append("\n");
            }

            jsonStream.close();
            atlas = sb.toString();

            Typeface commodore = Typeface.createFromAsset(assetManager, "fonts/commodore.ttf");

            Paint winnerPaint = new Paint();
            winnerPaint.setTypeface(commodore);
            winnerPaint.setTextAlign(Paint.Align.CENTER);
            winnerPaint.setTextSize(26);
            winnerPaint.setColor(Color.WHITE);
            winnerPaint.setAntiAlias(true);

        } catch (IOException e) {
            e.printStackTrace();
        }

        int offset = 32;

        for (int i = 1; i <= 5; i++) {
            horses.add(new Horse(xFactor, yFactor, 32, offset + 64 * i, i, sprite, atlas));
        }
    }

    @Override
    public boolean process(MotionEvent event) {
        hud.process(event);

        for (int i = 0; i < horses.size(); i++) {
            horses.get(i).process(event);
        }

        return true;
    }

    @Override
    public void draw(Canvas canvas) {

        for (int i = 0; i < horses.size(); i++) {
            horses.get(i).draw(canvas);
        }

        hud.draw(canvas);
    }

    @Override
    public void update(float deltaTime) {
        hud.update(deltaTime);

        switch (gameManager.state) {
            case Bet:
                break;
            case Race:
                for (int i = 0; i < horses.size(); i++) {
                    horses.get(i).update(deltaTime);
                }
                break;
            case RaceOver:
                gameManager.countRace += 1;

                for (int i = 0; i < horses.size(); i++) {
                    horses.get(i).resetPosition();
                }

                if (gameManager.winnerHorse == gameManager.betHorse) {
                    gameManager.money += gameManager.betAmount;
                } else {
                    gameManager.money -= gameManager.betAmount;
                }

                if (gameManager.countRace > gameManager.totalRaces) {
                    gameManager.state = GameState.GameOver;
                    break;
                }

                gameManager.betHorse = 0;
                gameManager.betAmount = 0;

                if (gameManager.money <= 0) {
                    gameManager.state = GameState.GameOver;
                    break;
                }

                gameManager.state = GameState.Bet;
                break;
            case GameOver:
                if (gameManager.money > 0.0f) {
                    SharedPreferences preferences = context.getSharedPreferences("FireRide", Context.MODE_PRIVATE);

                    Highscore highscore = new Highscore(gameManager.money, new Date().toString());
                    DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
                    reference.child("highscore").child(preferences.getString("uid", "")).child(System.currentTimeMillis() + "").setValue(highscore);
                }

                gameManager.reset();
                gameManager.state = GameState.Bet;
                break;
        }
    }
}
