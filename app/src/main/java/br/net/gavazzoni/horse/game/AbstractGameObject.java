package br.net.gavazzoni.horse.game;

import android.graphics.Canvas;
import android.view.MotionEvent;

abstract class AbstractGameObject {

    protected float xTouchFactor;
    protected float yTouchFactor;

    public AbstractGameObject(float xTouchFactor, float yTouchFactor) {
        this.xTouchFactor = xTouchFactor;
        this.yTouchFactor = yTouchFactor;
    }

    public abstract void update(float deltaTime);
    public abstract void draw(Canvas canvas);
    public abstract boolean process(MotionEvent motionEvent);
}
