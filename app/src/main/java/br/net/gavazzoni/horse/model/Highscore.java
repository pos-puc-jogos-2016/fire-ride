package br.net.gavazzoni.horse.model;

public class Highscore {
    public float money;
    public String created_at;

    public Highscore(){}

    public Highscore(float money, String created_at) {
        this.money = money;
        this.created_at = created_at;
    }

    @Override
    public String toString() {
        return "Highscore{" +
                "money=" + money +
                ", created_at='" + created_at + '\'' +
                '}';
    }
}
