package br.net.gavazzoni.horse;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;

import java.util.UUID;

import static android.view.WindowManager.LayoutParams.FLAG_FULLSCREEN;

public class MainActivity extends AppCompatActivity {

    private static final int HIGHSCORE_ACTIVITY = 1;
    private static final int GAME_ACTIVITY = 2;
    private static final int ABOUT_ACTIVITY = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(FLAG_FULLSCREEN, FLAG_FULLSCREEN);

        setContentView(R.layout.activity_main);


        SharedPreferences preferences = getSharedPreferences("FireRide", MODE_PRIVATE);

        if (!preferences.contains("uid")) {
            SharedPreferences.Editor editor = preferences.edit();
            String uniqueID = UUID.randomUUID().toString();
            editor.putString("uid", uniqueID);
            editor.apply();
        }
    }

    public void highscoreBtnClick(View view) {
        Intent intent  = new Intent(this, HighscoreActivity.class);

        startActivityForResult(intent, HIGHSCORE_ACTIVITY);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void gameBtnClick(View view) {
        Intent intent = new Intent(this, GameActivity.class);
        startActivityForResult(intent, GAME_ACTIVITY);
    }

    public void aboutBtnClick(View view) {
        Intent intent = new Intent(this, AboutActivity.class);
        startActivityForResult(intent, ABOUT_ACTIVITY);
    }
}
