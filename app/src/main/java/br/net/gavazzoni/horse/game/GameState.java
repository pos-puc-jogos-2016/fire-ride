package br.net.gavazzoni.horse.game;


public enum GameState {
    Bet,
    Race,
    RaceOver,
    GameOver
}
