package br.net.gavazzoni.horse;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import br.net.gavazzoni.horse.model.Highscore;

public class HighscoreActivity extends AppCompatActivity {

    private ListView listView;
    private ArrayList<Float> list = new ArrayList<>();
    private ArrayAdapter<Float> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_highscore);

        SharedPreferences preferences = getSharedPreferences("FireRide", MODE_PRIVATE);
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child("highscore").child(preferences.getString("uid", "")).getRef();

        listView = (ListView) findViewById(R.id.highscoreList);
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, list);
        listView.setAdapter(adapter);

        Log.d("FirebaseHighscore", databaseReference.toString());

        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Highscore highscore = null;
                for (DataSnapshot child: dataSnapshot.getChildren()) {
                    highscore = child.getValue(Highscore.class);

                    if (highscore != null) {
                        Log.d("FirebaseHighscore", highscore.toString());
                        list.add(highscore.money);

                    }
                }

                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w("Higshcore", "loadHighscore:onCancelled", databaseError.toException());
            }
        });
    }

    public void backBtnClick(View view) {
        setResult(RESULT_OK);
    }
}
