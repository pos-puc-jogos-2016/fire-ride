package br.net.gavazzoni.horse.game;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.util.Log;
import android.view.MotionEvent;

public class Button extends AbstractGameObject {

    private final Rect rect;
    private Paint paint = new Paint();
    private Paint textPaint = new Paint();
    private int color = Color.BLUE;
    private int textColor = Color.BLACK;
    private String text;
    private Point position = new Point();


    public Button(String text, Typeface font, int size, Point position, float xTouchFactor, float yTouchFactor) {
        super(xTouchFactor, yTouchFactor);

        this.text = text;
        this.position = position;

        textPaint.setTypeface(font);
        textPaint.setTextSize(size);

        Rect textBounds = new Rect();
        textPaint.getTextBounds(text, 0, text.length(), textBounds);
        rect = new Rect(position.x - (textBounds.width() / 2) - 10, position.y - (textBounds.height() / 2) - 10, position.x + (textBounds.width() / 2) + 10, position.y + 5 + (textBounds.height() / 2));
    }

    @Override
    public void update(float dateTime) {

    }

    @Override
    public void draw(Canvas canvas) {
        paint.setColor(color);
        textPaint.setColor(textColor);
        textPaint.setTextAlign(Paint.Align.CENTER);

        canvas.drawRect(rect, paint);
        canvas.drawText(text, position.x, position.y, textPaint);
    }

    @Override
    public boolean process(MotionEvent motionEvent) {
        float xTouch = motionEvent.getX() * xTouchFactor;
        float yTouch = motionEvent.getY() * yTouchFactor;

        return rect.contains((int) xTouch, (int) yTouch);
    }

    public Button setColor(int color) {
        this.color = color;
        return this;
    }

    public Button setTextColor(int textColor) {
        this.textColor = textColor;
        return this;
    }
}
