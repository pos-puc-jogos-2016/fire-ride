package br.net.gavazzoni.horse.game;

import android.app.Activity;
import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.view.MotionEvent;
import android.view.View;

import org.json.JSONException;


public class RenderView extends View {

    private long startTime;
    private Bitmap frameBuffer;
    private Canvas canvasBuffer;
    private Rect rect = new Rect();
    private MainScene mainScene;

    private GameState state;

    public RenderView(Context context, int width, int height, float xTouchFactor, float yTouchFactor) {
        super(context);

        startTime = System.nanoTime();
        GameManager.getInstance().screenSize = new Point(width, height);
        frameBuffer = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);

        AssetManager assetManager = getResources().getAssets();
        Typeface commodore = Typeface.createFromAsset(assetManager, "fonts/commodore.ttf");

        Paint textPaint = new Paint();
        textPaint.setTypeface(commodore);
        textPaint.setTextAlign(Paint.Align.CENTER);
        textPaint.setTextSize(14);
        textPaint.setColor(Color.WHITE);
        textPaint.setAntiAlias(true);

        canvasBuffer = new Canvas(frameBuffer);
        try {
            mainScene = new MainScene(xTouchFactor, yTouchFactor, textPaint, getContext());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        mainScene.process(event);

        return super.onTouchEvent(event);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (GameManager.getInstance().state == GameState.GameOver) {
            Activity activity = (Activity) getContext();
            activity.setResult(Activity.RESULT_OK);
            activity.finish();
        }

        float deltaTime = (System.nanoTime() - startTime) / 1000000.0f;
        startTime = System.nanoTime();

        rect.set(0, 0, canvas.getWidth(), canvas.getHeight());
        canvasBuffer.drawRGB(32, 139, 34);

        mainScene.update(deltaTime);
        mainScene.draw(canvasBuffer);

        canvas.drawBitmap(frameBuffer, null, rect, null);

        invalidate();
    }
}
