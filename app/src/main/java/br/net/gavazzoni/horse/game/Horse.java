package br.net.gavazzoni.horse.game;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.Rect;
import android.util.Log;
import android.view.MotionEvent;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Random;


public class Horse extends AbstractGameObject {

    private static final int width = 64;
    private static final int height = 64;
    private static final float scale = 0.6f;
    private static final int localFPS = 250;
    private static final int minVel = 10;
    private static final int maxVel = 80;
    private static final Point originalPosition = new Point();


    private Point position = new Point();
    private int index;
    private JSONObject spriteCoord;
    private Bitmap sprite;
    private Rect src = new Rect();
    private Rect dst = new Rect();
    private int frame = 0;
    private float elapsedTime = 0.0f;
    private Random rnd;

    public Horse(float xTouchFactor, float yTouchFactor, int x, int y, int index, Bitmap sprite, String atlas) throws JSONException {
        super(xTouchFactor, yTouchFactor);

        position.x = x;
        position.y = y;

        originalPosition.x = x;
        originalPosition.y = y;

        this.index = index;
        this.sprite = sprite;

        JSONArray jsonAtlas = new JSONArray(atlas);
        int spriteIndex = index - 1;

        if (spriteIndex >= jsonAtlas.length()) {
            spriteIndex = 0;
        }

        if (spriteIndex < 0) {
            spriteIndex = 0;
        }


        spriteCoord = jsonAtlas.getJSONObject(spriteIndex);

        rnd = new Random();
    }

    @Override
    public void update(float deltaTime) {
        elapsedTime += deltaTime;

        if (elapsedTime > localFPS) {
            elapsedTime = 0;
            frame++;
            if (frame > 3) {
                frame = 0;
            }
        }

        int vel = rnd.nextInt((maxVel - minVel) + 1) + minVel;
        position.x += (vel * deltaTime) / 1000.0f;

        if (position.x > GameManager.getInstance().screenSize.x) {
            GameManager.getInstance().winnerHorse = index;
            GameManager.getInstance().state = GameState.RaceOver;
        }
    }

    @Override
    public void draw(Canvas canvas) {
        try {
            JSONArray frameCoordArray = spriteCoord.getJSONArray("right");
            JSONArray frameCoord = frameCoordArray.getJSONArray(frame);

            src.set(frameCoord.getInt(0), frameCoord.getInt(1), frameCoord.getInt(2), frameCoord.getInt(3));
            dst.set((int) ((position.x - (width / 2)) * scale), (int) ((position.y - (height / 2)) * scale), (int) ((position.x + (width / 2)) * scale), (int) ((position.y + (height / 2)) * scale));

            canvas.drawBitmap(sprite, src, dst, null);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean process(MotionEvent motionEvent) {

        if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
            float xTouch = motionEvent.getX() * xTouchFactor;
            float yTouch = motionEvent.getY() * yTouchFactor;

            if (dst.contains((int) xTouch, (int) yTouch)) {
                if (GameManager.getInstance().betHorse != index) {
                    GameManager.getInstance().betAmount = 0.0f;
                }

                float nextAmount = GameManager.getInstance().betAmount + 50.0f;

                if (nextAmount <= GameManager.getInstance().money) {
                    GameManager.getInstance().betHorse = index;
                    GameManager.getInstance().betAmount = nextAmount;
                }
            }
        }

        return true;
    }

    void resetPosition() {
        position.x = originalPosition.x;
    }
}
