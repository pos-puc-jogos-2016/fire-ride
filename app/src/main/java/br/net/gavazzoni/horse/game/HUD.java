package br.net.gavazzoni.horse.game;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.view.MotionEvent;

import java.text.NumberFormat;

public class HUD extends AbstractScene {

    private Button startButton;
    private Button clearButton;

    public HUD(float xFactor, float yFactor, Paint textPaint) {
        super(xFactor, yFactor, textPaint);

        Point pointStart = new Point(120, 14);
        Point pointClear = new Point(200, 14);
        startButton = new Button("Correr", textPaint.getTypeface(), 10, pointStart, xFactor, yFactor);

        startButton.setTextColor(Color.BLACK)
                .setColor(Color.YELLOW)
        ;

        clearButton = new Button("Limpar aposta", textPaint.getTypeface(), 10, pointClear, xFactor, yFactor);
        clearButton.setTextColor(Color.BLACK)
                .setTextColor(Color.YELLOW);
    }

    @Override
    public void draw(Canvas canvas) {
        String text = "Corrida " + GameManager.getInstance().countRace + "/" + GameManager.getInstance().totalRaces;
        Rect textBounds = new Rect();

        textPaint.getTextBounds(text, 0, text.length(), textBounds);
        canvas.drawText(text, textBounds.width() / 2, textBounds.height(), textPaint);


        Rect moneyTextBounds = new Rect();
        Rect betTextBounds = new Rect();
        Rect betHorseBounds = new Rect();

        float money = GameManager.getInstance().money;
        float bet = GameManager.getInstance().betAmount;

        textPaint.getTextBounds(formatMoney(money), 0, formatMoney(money).length(), moneyTextBounds);
        canvas.drawText(formatMoney(money), 320 - (moneyTextBounds.width() / 2), moneyTextBounds.height(), textPaint);

        textPaint.getTextBounds(formatMoney(bet), 0, formatMoney(bet).length(), betTextBounds);
        canvas.drawText(formatMoney(bet), 320 - (betTextBounds.width() / 2), betTextBounds.height() + moneyTextBounds.height() + 10, textPaint);

        if (GameManager.getInstance().winnerHorse > 0) {
            text = "Cavalo " + GameManager.getInstance().winnerHorse + " venceu";
            canvas.drawText(text, 160, 120, textPaint);
        }

        if (GameManager.getInstance().betHorse > 0) {
            text = "Selecionado: " + GameManager.getInstance().betHorse;
            textPaint.getTextBounds(text, 0, text.length(), betHorseBounds);
            canvas.drawText(text, 5 + (betHorseBounds.width() / 2), textBounds.height() + 10 + betHorseBounds.height(), textPaint);
        }

        startButton.draw(canvas);
        clearButton.draw(canvas);
    }

    @Override
    public void update(float deltaTime) {

    }

    @Override
    public boolean process(MotionEvent event) {

        if (GameManager.getInstance().state == GameState.Bet) {
            if (startButton.process(event) && GameManager.getInstance().betAmount > 0.0f) {
                GameManager.getInstance().state = GameState.Race;
                GameManager.getInstance().winnerHorse = 0;
            }

            if (clearButton.process(event)) {
                GameManager.getInstance().betAmount = 0;
                GameManager.getInstance().betHorse = 0;
            }
        }


        return true;
    }

    private String formatMoney(float money) {
        NumberFormat format = NumberFormat.getCurrencyInstance();
        return format.format(money);
    }
}
