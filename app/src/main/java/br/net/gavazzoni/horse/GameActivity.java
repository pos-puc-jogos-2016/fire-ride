package br.net.gavazzoni.horse;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import br.net.gavazzoni.horse.game.RenderView;

import static android.view.WindowManager.LayoutParams.FLAG_FULLSCREEN;

public class GameActivity extends AppCompatActivity implements View.OnTouchListener {

    RenderView renderView;
    private static final int WIDTH = 320;
    private static final int HEIGHT = 240;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        float xTouchFactor = (float) WIDTH / metrics.widthPixels;
        float yTouchFactor = (float) HEIGHT / metrics.heightPixels;

        renderView = new RenderView(this, WIDTH, HEIGHT, xTouchFactor, yTouchFactor);
        renderView.setOnTouchListener(this);
        setContentView(renderView);

        getWindow().setFlags(FLAG_FULLSCREEN, FLAG_FULLSCREEN);
    }



    @Override
    public boolean onTouch(View v, MotionEvent event) {
        return renderView.onTouchEvent(event);
    }
}
