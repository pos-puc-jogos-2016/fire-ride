Fire Ride
=========


* Corrida de cavalos onde o jogador aposta dinheiro.

* Objetivo do jogo é ganhar corridas e não perder dinheiro.

* Voce pode escolher 1 entre 5 cavalos para apostar.

* O jogador começa com 1000 Dinheiros e se o dinheiro chegar a 0, acaba o jogo.

* Tem 5 corridas para conseguir ganhar o máximo de dinheiro possivel.

* O jogador pode apostar qualquer valor e o premio será baseado no valor da aposta.

* O jogo terá um placar local.



Repositório Git: https://gitlab.com/pos-puc-jogos-2016/fire-ride


Playing
=======

* Clique no cavalo para apostar nele.
* Apos selecionar o cavalo, clique em "Correr"
* Se o cavalo selecionado for o vencedor, receberá o valor da aposta
* Se o cavalo selecionado perder, perderá o valor da aposta
